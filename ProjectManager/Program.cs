﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManager
{
    internal class Program
    {
        private static void Main()
        {
            ConfigureSignalR();
            foreach (var message in Api.GetAllRecords().Result)
            {
                Console.WriteLine($"{message.DateRecieved.ToString("F")}: {message.Message}");
            }
            
            //TestRequests();

            var tasks = new List<Task<int>> { TestAsyncQueries(), TestAsyncQueries(), TestAsyncQueries(), TestAsyncQueries() };
            var all = System.Threading.Tasks.Task.WhenAll(tasks);
            foreach(var result in all.Result)
            {
                Console.WriteLine(result);
            }

            Console.ReadKey();
        }

        private static async Task<int> TestAsyncQueries()
        {
            return await Queires.MarkRandomTaskWithDelay(1000);
        }

        private static void TestRequests()
        {
            var task1 = ApiRequests.GetTaskCount(5);
            //var task2 = ApiRequests.GetUserTasksWithNameLengthLessThan45(3);
            var task3 = ApiRequests.GetTasksFinishedIn2019(15);
            var task4 = ApiRequests.GetTeamsWithUsersOlderThan12();
            //var task5 = ApiRequests.GetUserStats(12);
            var task6 = ApiRequests.GetProjectStats(11);

            var tasks = new System.Threading.Tasks.Task[] { task1, /* task2, */ task3, task4, /* task5, */ task6 };
            var completedTasks = System.Threading.Tasks.Task.WhenAll(tasks);

            try
            {
                completedTasks.Wait();
            } catch (AggregateException ex)
            {
                Console.WriteLine($"Exception {ex.InnerException}: {ex.InnerException.Message}");
            }

            if (completedTasks.Status == TaskStatus.RanToCompletion)
                Console.WriteLine("All tasks succeeded.");
            else if (completedTasks.Status == TaskStatus.Faulted)
                Console.WriteLine("There are failed tasks");

        }
        private static async void ConfigureSignalR()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            var connection = new HubConnectionBuilder()
                .WithUrl(configuration.GetSection("HubUri").Value)
                .Build();

            await connection.StartAsync();

            connection.On<string>("RecieveMessage", OnMessage);

        }
        private static void OnMessage(string message)
        {
            var record = JsonConvert.DeserializeObject<ProjectShared.Entities.StatusRecord>(message);
            string output = record.Success ? $"Recieved request success confirmation: {record.Message}" : $"Recieved request error: {record.Message}";
            Console.WriteLine(output);
        }
    }
}