﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectWorker.Interfaces;
using ProjectWorker.Services;
using System;
using System.IO;

namespace ProjectWorker
{
    class Program
    {
        public static IConfiguration Configuration;
        private static IServiceProvider _services;
        static void Main(string[] args)
        {
            Configure();
            ConfigureServices();
            using (var appHost = _services.GetService<IMessageService>())
            {
                appHost.Run();
                Console.WriteLine("Press Escape to close application");
                while(Console.ReadKey(true).Key != ConsoleKey.Escape) { }
                return;
            }
            

        }

        private static void ConfigureServices()
        {
            _services = new ServiceCollection()
            .AddSingleton(provider => Configuration)

            .AddScoped<IMessageService, MessageService>()
            .AddScoped<IFileService, FileService>()
            .AddScoped<IQueueService, QueueService>()
            .BuildServiceProvider();
        }
        private static void Configure()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            Configuration = builder.Build();
        }

    }
}
