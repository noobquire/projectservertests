﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectWorker.Interfaces
{
    public interface IQueueService
    {
        bool PostValue(string message);

    }
}
