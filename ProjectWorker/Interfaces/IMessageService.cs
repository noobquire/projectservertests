﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorker.Interfaces
{
    public interface IMessageService : IDisposable
    {
        void Run();
    }
}
