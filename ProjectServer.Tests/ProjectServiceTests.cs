﻿using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using ProjectServer.DAL;
using ProjectServer.Interfaces;
using ProjectServer.MappingProfiles;
using ProjectServer.Services;
using ProjectShared.DTO;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.InMemory;
using System.Text;

namespace ProjectServer.Tests
{
    class ProjectServiceTests
    {
        private IUnitOfWork unitOfWork;
        private IMapper mapper;
        private IQueueService queueService;

        [SetUp]
        public void Setup()
        {
            // Arrange
            var team1 = new Team { Id = 0, CreatedAt = new DateTime(2017, 03, 03), Name = "Test team 1" };
            var team2 = new Team { Id = 1, CreatedAt = new DateTime(2018, 01, 04), Name = "Test team 2" };

            var user1 = new User
            {
                Id = 0,
                Birthday = new DateTime(1998, 07, 03),
                Email = "test1@gmail.com",
                FirstName = "John",
                LastName = "Doe",
                RegisteredAt = new DateTime(2016, 07, 05),
                Team = team1
            };
            var user2 = new User
            {
                Id = 1,
                Birthday = new DateTime(1996, 03, 01),
                Email = "test2@gmail.com",
                FirstName = "Jane",
                LastName = "Doe",
                RegisteredAt = new DateTime(2017, 04, 03),
                Team = team2
            };

            var project1 = new Project
            {
                Id = 0,
                Author = user2,
                StartedAt = new DateTime(2018, 03, 07),
                Deadline = new DateTime(2019, 05, 06),
                Name = "Very first project",
                Description = "Do some stuff here.",
                Team = team2
            };
            var project2 = new Project
            {
                Id = 1,
                Author = user1,
                StartedAt = new DateTime(2018, 05, 01),
                Deadline = new DateTime(2019, 07, 04),
                Name = "Second project",
                Description = "Do some stuff, too.",
                Team = team1
            };

            var userproject1 = new UserProject { User = user1, Project = project2, ProjectId = 1, UserId = 0 };
            var userproject2 = new UserProject { User = user2, Project = project1, ProjectId = 0, UserId = 1 };

            project1.Users = new List<UserProject> { userproject2 };
            project2.Users = new List<UserProject> { userproject1 };

            user1.Projects = new List<UserProject> { userproject1 };
            user2.Projects = new List<UserProject> { userproject2 };

            var task1 = new Task
            {
                Id = 0,
                CompletedAt = new DateTime(2018, 06, 03),
                CreatedAt = new DateTime(2018, 05, 06),
                Description = "Add a new feature 1",
                Name = "Task 1 a. This is a lot of symbols probably??",
                Performer = user1,
                Project = project2,
                State = TaskState.Finished
            };
            var task2 = new Task
            {
                Id = 1,
                CompletedAt = new DateTime(2019, 07, 05),
                CreatedAt = new DateTime(2018, 06, 09),
                Description = "Add a new feature 2",
                Name = "Task 2 ab",
                Performer = user2,
                Project = project1,
                State = TaskState.Cancelled
            };
            var task3 = new Task
            {
                Id = 2,
                CompletedAt = new DateTime(2019, 12, 06),
                CreatedAt = new DateTime(2018, 06, 27),
                Description = "Add a new feature 3",
                Name = "Task 3 abc",
                Performer = user2,
                Project = project1,
                State = TaskState.Created
            };
            var task4 = new Task
            {
                Id = 3,
                CompletedAt = new DateTime(2018, 11, 04),
                CreatedAt = new DateTime(2018, 09, 1),
                Description = "Add a new feature 4. Can we please get more than 45 symbols? Please?",
                Name = "Task 4 abcd",
                Performer = user1,
                Project = project2,
                State = TaskState.Started
            };

            unitOfWork = A.Fake<IUnitOfWork>();
            A.CallTo(() => unitOfWork.Projects.GetAllAsync())
                .Returns(System.Threading.Tasks.Task.FromResult((IEnumerable<Project>)new[] { project1, project2 }));
            A.CallTo(() => unitOfWork.Tasks.GetAllAsync())
                .Returns(System.Threading.Tasks.Task.FromResult((IEnumerable<Task>)new[] { task1, task2, task3, task4 }));
            A.CallTo(() => unitOfWork.Users.GetAllAsync())
                .Returns(System.Threading.Tasks.Task.FromResult((IEnumerable<User>)new[] { user1, user2 }));
            A.CallTo(() => unitOfWork.Teams.GetAllAsync())
                .Returns(System.Threading.Tasks.Task.FromResult((IEnumerable<Team>)new[] { team1, team2 }));

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });
            mapper = config.CreateMapper();

            queueService = A.Fake<IQueueService>();
        }
        [Test]
        public async System.Threading.Tasks.Task GetProjectStatsAsync_ShouldReturnCorrectStats()
        {
            var projectsService = new ProjectsService(unitOfWork, mapper, queueService);

            var result = await projectsService.GetProjectStatsAsync(1);

            var expected = new ProjectStatsDTO { ProjectId = 1, TaskWithLongestDescId = 3, TaskWithShortestNameId = 3, TotalUsersInProjectTeam = 1 };

            Assert.AreEqual(expected, result);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_ShouldCallRepositoryCreate_WhenCreatingProject()
        {
            var options = new DbContextOptionsBuilder<ProjectServerDbContext>().UseInMemoryDatabase(databaseName: "ProjectServerInMemory").Options;

            using (var context = new ProjectServerDbContext(options))
            {

                unitOfWork = new UnitOfWork(new DbProjectRepository(context), new DbTaskRepository(context), new DbTeamRepository(context), new DbUserRepository(context), context);
                var projectsService = new ProjectsService(unitOfWork, mapper, queueService);
                await projectsService.CreateAsync(new ProjectDTO { Id = 123, Name = "Test Adding Projects" });
            }
            using (var context = new ProjectServerDbContext(options))
            {
                Assert.AreEqual(1, await context.Projects.CountAsync());
                Assert.AreEqual(123, (await context.Projects.SingleAsync()).Id);
            }

        }
    }
}
