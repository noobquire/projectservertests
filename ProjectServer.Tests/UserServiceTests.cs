using NUnit.Framework;
using FakeItEasy;
using ProjectServer.Interfaces;
using ProjectShared.DTO;
using System;
using AutoMapper;
using ProjectShared.Entities;
using System.Collections.Generic;
using ProjectServer.Services;
using ProjectServer.MappingProfiles;
using ProjectServer.DAL;
using Microsoft.EntityFrameworkCore;

namespace ProjectServer.Tests
{
    public class UserServiceTests
    {
        private IUnitOfWork unitOfWork;
        private IMapper mapper;
        private IQueueService queueService;

        [SetUp]
        public void Setup()
        {
            // Arrange
            var team1 = new Team { Id = 0, CreatedAt = new DateTime(2017, 03, 03), Name = "Test team 1" };
            var team2 = new Team { Id = 1, CreatedAt = new DateTime(2018, 01, 04), Name = "Test team 2" };

            var user1 = new User
            {
                Id = 0,
                Birthday = new DateTime(1998, 07, 03),
                Email = "test1@gmail.com",
                FirstName = "John",
                LastName = "Doe",
                RegisteredAt = new DateTime(2016, 07, 05),
                Team = team1
            };
            var user2 = new User
            {
                Id = 1,
                Birthday = new DateTime(1996, 03, 01),
                Email = "test2@gmail.com",
                FirstName = "Jane",
                LastName = "Doe",
                RegisteredAt = new DateTime(2017, 04, 03),
                Team = team2
            };

            var project1 = new Project
            {
                Id = 0,
                Author = user2,
                StartedAt = new DateTime(2018, 03, 07),
                Deadline = new DateTime(2019, 05, 06),
                Name = "Very first project",
                Description = "Do some stuff here.",
                Team = team2
            };
            var project2 = new Project
            {
                Id = 1,
                Author = user1,
                StartedAt = new DateTime(2018, 05, 01),
                Deadline = new DateTime(2019, 07, 04),
                Name = "Second project",
                Description = "Do some stuff, too.",
                Team = team1
            };

            var userproject1 = new UserProject { User = user1, Project = project2, ProjectId = 1, UserId = 0 };
            var userproject2 = new UserProject { User = user2, Project = project1, ProjectId = 0, UserId = 1 };

            project1.Users = new List<UserProject> { userproject2 };
            project2.Users = new List<UserProject> { userproject1 };

            user1.Projects = new List<UserProject> { userproject1 };
            user2.Projects = new List<UserProject> { userproject2 };

            var task1 = new Task
            {
                Id = 0,
                CompletedAt = new DateTime(2018, 06, 03),
                CreatedAt = new DateTime(2018, 05, 06),
                Description = "Add a new feature 1",
                Name = "Task 1 a. This is a lot of symbols probably??",
                Performer = user1,
                Project = project2,
                State = TaskState.Finished
            };
            var task2 = new Task
            {
                Id = 1,
                CompletedAt = new DateTime(2019, 07, 05),
                CreatedAt = new DateTime(2018, 06, 09),
                Description = "Add a new feature 2",
                Name = "Task 2 ab",
                Performer = user2,
                Project = project1,
                State = TaskState.Cancelled
            };
            var task3 = new Task
            {
                Id = 2,
                CompletedAt = new DateTime(2019, 12, 06),
                CreatedAt = new DateTime(2018, 06, 27),
                Description = "Add a new feature 3",
                Name = "Task 3 abc",
                Performer = user2,
                Project = project1,
                State = TaskState.Created
            };
            var task4 = new Task
            {
                Id = 3,
                CompletedAt = new DateTime(2018, 11, 04),
                CreatedAt = new DateTime(2018, 09, 1),
                Description = "Add a new feature 4. Can we please get more than 45 symbols? Please?",
                Name = "Task 4 abcd",
                Performer = user1,
                Project = project2,
                State = TaskState.Started
            };

            unitOfWork = A.Fake<IUnitOfWork>();
            A.CallTo(() => unitOfWork.Projects.GetAllAsync())
                .Returns(System.Threading.Tasks.Task.FromResult((IEnumerable<Project>)new[] { project1, project2 }));
            A.CallTo(() => unitOfWork.Tasks.GetAllAsync())
                .Returns(System.Threading.Tasks.Task.FromResult((IEnumerable<Task>)new[] { task1, task2, task3, task4 }));
            A.CallTo(() => unitOfWork.Users.GetAllAsync())
                .Returns(System.Threading.Tasks.Task.FromResult((IEnumerable<User>)new[] { user1, user2 }));
            A.CallTo(() => unitOfWork.Teams.GetAllAsync())
                .Returns(System.Threading.Tasks.Task.FromResult((IEnumerable<Team>)new[] { team1, team2 }));

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });
            mapper = config.CreateMapper();

            queueService = A.Fake<IQueueService>();
        }

        [Test]
        public async System.Threading.Tasks.Task GetTaskCountAsync_ShouldReturnCorrectCount_WhenGivenProjectAuthor()
        {
            var usersService = new UsersService(unitOfWork, mapper, queueService);

            var result = await usersService.GetTaskCountAsync(0);

            var expected = new Dictionary<int, int>() { [1] = 2 };
            CollectionAssert.AreEquivalent(expected, result);
        }

        [Test]
        public async System.Threading.Tasks.Task GetTasksFinishedIn2019Async_ShouldReturnEmpty_WhenTasksAreNotFinished()
        {
            var usersService = new UsersService(unitOfWork, mapper, queueService);

            var result = await usersService.GetTasksFinishedIn2019Async(1);

            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public async System.Threading.Tasks.Task GetUsersSortedByFirstNameWithTasksSortedByNameLengthAsync_ShouldReturnUsersAndTasksInCorrectOrder()
        {
            var usersService = new UsersService(unitOfWork, mapper, queueService);

            var result = await usersService.GetUsersSortedByFirstNameWithTasksSortedByNameLengthAsync();

            var expected = new[] { new TasksOfUserDTO { UserId = 1, TasksOfUserIds = new[] { 2, 1 } }, new TasksOfUserDTO { UserId = 0, TasksOfUserIds = new[] { 0, 3 } } };

            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public async System.Threading.Tasks.Task GetUserStatsAsync_ShouldReturnCorrectStats()
        {
            var usersService = new UsersService(unitOfWork, mapper, queueService);

            var result = await usersService.GetUserStatsAsync(1);

            var expected = new UserStatsDTO { UserId = 1, LastProjectId = 0, CancelledAndUnfinishedTasksCount = 2, LastProjectTasksCount = 2, LongestTaskId = 2 };

            Assert.AreEqual(expected, result);
        }

        [Test]
        public async System.Threading.Tasks.Task GetTasksWithNameLengthLessThan45Async_ShouldNotReturnTasksWithNameLengthMoreThan45()
        {

            var usersService = new UsersService(unitOfWork, mapper, queueService);

            var result = await usersService.GetTasksWithNameLengthLessThan45Async(0);

            var expected = new[] {
                new TaskDTO {
                    Id = 3,
                    CreatedAt = new DateTime(2018, 09, 1),
                    FinishedAt = new DateTime(2018, 11, 04),
                    Description = "Add a new feature 4. Can we please get more than 45 symbols? Please?",
                    Name = "Task 4 abcd",
                    PerformerId = 0,
                    ProjectId = 1,
                    State = TaskState.Started,
                } };
            Assert.AreEqual(expected, result);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_ShouldCreateUserInRepository_WhenCreatingNewUser()
        {
            var usersService = new UsersService(unitOfWork, mapper, queueService);

            await usersService.CreateAsync(A.Fake<UserDTO>());

            A.CallTo(() => unitOfWork.Users.CreateAsync(A<User>._)).MustHaveHappenedOnceExactly();
        }

        [TearDown]
        public void Teardown()
        {
            var options = new DbContextOptionsBuilder<ProjectServerDbContext>().UseInMemoryDatabase(databaseName: "ProjectServerInMemory").Options;
            using (var context = new ProjectServerDbContext(options))
            {
                foreach (var entity in context.Users)
                    context.Users.Remove(entity);
                context.SaveChanges();
            }
        }

        [Test]
        public async System.Threading.Tasks.Task DeleteAsync_ShouldDeleteUserFromDatabase_WhenDeletingUser()
        {
            var options = new DbContextOptionsBuilder<ProjectServerDbContext>().UseInMemoryDatabase(databaseName: "ProjectServerInMemory").Options;

            using (var context = new ProjectServerDbContext(options))
            {

                unitOfWork = new UnitOfWork(new DbProjectRepository(context), new DbTaskRepository(context), new DbTeamRepository(context), new DbUserRepository(context), context);
                var usersService = new UsersService(unitOfWork, mapper, queueService);
                await usersService.CreateManyAsync(new[] { new UserDTO() { Id = 123 }, new UserDTO { Id = 456 }, new UserDTO { Id = 789 } });
                await usersService.DeleteAsync(123);
                Assert.AreEqual(2, await context.Users.CountAsync());
            }
        }
    }
}