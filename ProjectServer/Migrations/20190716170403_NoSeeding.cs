﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectServer.Migrations
{
    public partial class NoSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                maxLength: 30,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tasks",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Tasks",
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Projects",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 30);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 150);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Projects",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 1000,
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "Deadline", "Description", "Name", "StartedAt", "TeamId" },
                values: new object[,]
                {
                    { 1, null, new DateTime(2021, 5, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "We are doing some real stuff here", "First Project", new DateTime(2017, 4, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 2, null, new DateTime(2019, 6, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Let's have some fun!", "Project number two", new DateTime(2018, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 3, null, new DateTime(2017, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Together we can change the world, just one random act of kindness at a time.", "This is our third project", new DateTime(2016, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 4, null, new DateTime(2023, 1, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Someone that you obviously can trust and someone that you would trust your life with - that's a really good friend.", "Fourth and biggest project to this day", new DateTime(2013, 9, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), null }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CompletedAt", "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 7, new DateTime(2019, 9, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 7, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Etiam gravida lorem orci, et faucibus lorem mollis quis. Sed hendrerit arcu nisl, cursus pulvinar tellus luctus vitae. Integer at ultricies ante.", "Nunc eu cursus libero, nec tempus mi", null, null, 2 },
                    { 6, new DateTime(2019, 7, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 4, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "Donec tristique luctus elit sit amet luctus. Pellentesque lacus felis, pulvinar nec libero a, egestas semper justo.", "Ut ut tincidunt sapien, sed convallis mi.", null, null, 2 },
                    { 4, new DateTime(2018, 6, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2018, 3, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nunc eu cursus libero, nec tempus mi. Etiam gravida lorem orci, et faucibus lorem mollis quis. Sed hendrerit arcu nisl, cursus pulvinar tellus luctus vitae.", "Aliquam erat volutpat", null, null, 2 },
                    { 5, new DateTime(2019, 3, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 1, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Maecenas lacinia facilisis mauris, eget feugiat arcu ornare quis. Nulla nisi lectus, eleifend id eros sit amet, mollis ultrices erat. Etiam vel aliquet libero.", "Vivamus purus ante, venenatis id", null, null, 2 },
                    { 2, new DateTime(2017, 6, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2017, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gute irure dolor in reprehenderit in voluptate velit aute irure dolor in reprehenderit.", "In reprehenderit consectetur dolor in voluptate velit esse", null, null, 2 },
                    { 1, new DateTime(2017, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2017, 12, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.", "Consectetur adipiscing elit, sed do eiusmod tempor", null, null, 2 },
                    { 3, new DateTime(2018, 4, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2018, 3, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sed dapibus mi rhoncus purus tristique, dignissim efficitur est tristique.", "Donec vitae augue ex", null, null, 2 }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2015, 3, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Team Green" },
                    { 2, new DateTime(2016, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Team Grey" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 4, new DateTime(1995, 9, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "appvich@gmail.com", "Alexey", "Popovich", new DateTime(2016, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 1, new DateTime(1996, 7, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "johndoe@gmail.com", "John", "Doe", new DateTime(2015, 3, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 2, new DateTime(1998, 1, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "vpoopkin@gmail.com", "Vasya", "Poopkin", new DateTime(2015, 5, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 3, new DateTime(1990, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "imurms@gmail.com", "Ilya", "Muromets", new DateTime(2015, 5, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 5, new DateTime(1989, 4, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "ltolstoy@gmail.com", "Lev", "Tolstoy", new DateTime(2016, 8, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), null }
                });
        }
    }
}
