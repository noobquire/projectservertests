﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectShared.DTO;
using ProjectServer.DAL;
using ProjectServer.Interfaces;
using System.Threading.Tasks;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;

        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        // GET: api/Tasks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Get()
        {
            return Ok(await _tasksService.GetAllAsync());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            return Ok(await _tasksService.GetAsync(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TaskDTO value)
        {
            await _tasksService.CreateAsync(value);
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public async Task<ActionResult> Post([FromBody] IEnumerable<TaskDTO> values)
        {
            await _tasksService.CreateManyAsync(values);
            return Ok();
        }

        // PUT: api/Tasks
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TaskDTO value)
        {
            await _tasksService.UpdateAsync(value);
            return Ok();
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _tasksService.DeleteAsync(id);
            return Ok();
        }
    }
}