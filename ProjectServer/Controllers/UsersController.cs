﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectShared.DTO;
using ProjectServer.DAL;
using ProjectServer.Interfaces;
using System.Threading.Tasks;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UsersController(IUnitOfWork unitOfWork, IUsersService usersService)
        {
            _usersService = usersService;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            return Ok(await _usersService.GetAllAsync());
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            return Ok(await _usersService.GetAsync(id));
        }

        // POST: api/Users
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] UserDTO value)
        {
            await _usersService.CreateAsync(value);
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public async Task<ActionResult> Post([FromBody] IEnumerable<UserDTO> values)
        {
            await _usersService.CreateManyAsync(values);
            return Ok();
        }
        
        // PUT: api/Users
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UserDTO value)
        {
            await _usersService.UpdateAsync(value);
            return Ok();
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _usersService.DeleteAsync(id);
            return Ok();
        }

        // GET: api/Users/5/TaskCount
        [HttpGet]
        [Route("{id}/TaskCount")]
        public async Task<ActionResult<Dictionary<ProjectDTO, int>>> GetTaskCount(int id)
        {
            return Ok(await _usersService.GetTaskCountAsync(id));
        }

        // GET: api/Users/5/TasksWithNameLengthLessThan45
        [HttpGet]
        [Route("{id}/TasksWithNameLengthLessThan45")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasksWithNameLengthLessThan45(int id)
        {
            return Ok(await _usersService.GetTasksWithNameLengthLessThan45Async(id));
        }

        // GET: api/Users/5/TasksFinishedIn2019
        [HttpGet]
        [Route("{id}/TasksFinishedIn2019")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasksFinishedIn2019(int id)
        {
            
            return Ok(await _usersService.GetTasksFinishedIn2019Async(id));
        }

        // GET: api/Users/5/Stats
        [HttpGet]
        [Route("{id}/Stats")]
        public async Task<ActionResult<UserStatsDTO>> GetUserStats(int id)
        {
            
            return Ok(await _usersService.GetUserStatsAsync(id));
        }

        // GET: api/Users/UsersSortedByFirstNameWithTasksSortedByNameLength
        [HttpGet]
        [Route("SortedByFirstNameWithTasksSortedByNameLength")]
        public async Task<ActionResult<IEnumerable<TasksOfUserDTO>>> GetUsersSortedByFirstNameWithTasksSortedByNameLength()
        {
            
            return Ok(await _usersService.GetUsersSortedByFirstNameWithTasksSortedByNameLengthAsync());
        }
    }
}