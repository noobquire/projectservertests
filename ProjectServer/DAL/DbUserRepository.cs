﻿using Microsoft.EntityFrameworkCore;
using ProjectServer.Interfaces;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectServer.DAL
{
    public class DbUserRepository : IRepository<User>
    {
        private ProjectServerDbContext _context;
        public DbUserRepository(ProjectServerDbContext context)
        {
            _context = context;
        }
        public async System.Threading.Tasks.Task CreateAsync(User item)
        {
            if (_context.Users.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            await _context.Users.AddAsync(item);
        }

        public async System.Threading.Tasks.Task CreateAsync(IEnumerable<User> items, CancellationToken cancellationToken = default)
        {
            foreach (var item in items)
            {
                await CreateAsync(item);
                if (cancellationToken.IsCancellationRequested) break;
            }
        }
        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _context.Users.Remove(await GetAsync(id));
        }

        public async Task<User> GetAsync(int id)
        {
            return (await _context.Users.ToListAsync()).Single(p => p.Id == id);
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public async System.Threading.Tasks.Task UpdateAsync(User item)
        {
            var user = await GetAsync(item.Id);
            user = item;
        }
    }
}
