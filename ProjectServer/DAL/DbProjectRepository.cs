﻿using Microsoft.EntityFrameworkCore;
using ProjectServer.Interfaces;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectServer.DAL
{
    public class DbProjectRepository : IRepository<Project>
    {
        private ProjectServerDbContext _context;
        public DbProjectRepository(ProjectServerDbContext context)
        {
            _context = context;
        }

        public async System.Threading.Tasks.Task CreateAsync(Project item)
        {
            if (_context.Projects.AsParallel().Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            //await _context.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Projects ON");
            
            await _context.Projects.AddAsync(item);
            //await _context.Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.Projects OFF");
        }

        public async System.Threading.Tasks.Task CreateAsync(IEnumerable<Project> items, CancellationToken cancellationToken = default)
        {
            foreach (var item in items)
            {
                await CreateAsync(item);
                if (cancellationToken.IsCancellationRequested) break;
            }
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _context.Projects.Remove(await GetAsync(id));
        }

        public async Task<Project> GetAsync(int id)
        {
            return (await _context.Projects.ToListAsync()).Single(p => p.Id == id);
        }

        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            return await _context.Projects.ToListAsync();
        }

        public async System.Threading.Tasks.Task UpdateAsync(Project item)
        {
            var project = await GetAsync(item.Id);
            project = item;
        }
    }
}
