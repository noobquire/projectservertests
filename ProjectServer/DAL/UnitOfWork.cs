﻿using ProjectShared.DTO;
using ProjectServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.Entities;
using Microsoft.EntityFrameworkCore;

namespace ProjectServer.DAL
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private ProjectServerDbContext _context;

        public UnitOfWork(IRepository<Project> projects, IRepository<Task> tasks, IRepository<Team> teams, IRepository<User> users, ProjectServerDbContext context)
        {
            Projects = projects;
            Tasks = tasks;
            Teams = teams;
            Users = users;
            _context = context;
        }

        public IRepository<Project> Projects { get; }

        public IRepository<Task> Tasks { get; }

        public IRepository<Team> Teams { get; }

        public IRepository<User> Users { get; }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async System.Threading.Tasks.Task SaveChangesAsync()
        {
           await _context.SaveChangesAsync();
        }
    }
}
