﻿using System;
using Newtonsoft.Json;

namespace ProjectShared.DTO
{
    [Serializable]
    public class ProjectDTO
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty(PropertyName = "deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty(PropertyName = "author_id")]
        public int AuthorId { get; set; }

        [JsonProperty(PropertyName = "team_id")]
        public int TeamId { get; set; }
    }
}