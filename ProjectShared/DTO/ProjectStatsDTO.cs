﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectShared.DTO
{
    [Serializable]
    public struct ProjectStatsDTO
    {
        [JsonProperty(PropertyName = "project_id")]
        public int ProjectId { get; set; }
        [JsonProperty(PropertyName = "task_with_longest_desc_id")]
        public int TaskWithLongestDescId { get; set; }
        [JsonProperty(PropertyName = "task_with_shortest_name_id")]
        public int TaskWithShortestNameId { get; set; }
        /// <summary>
        /// Amount of users which are in team of this project. 
        /// Zero (even if there are users in team),
        /// if either project description is less than 25 or amount of tasks is less than 3.
        /// </summary>
        [JsonProperty(PropertyName = "total_users_in_project_team")]
        public int TotalUsersInProjectTeam { get; set; }
    }
}
