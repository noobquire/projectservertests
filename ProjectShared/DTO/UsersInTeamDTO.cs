﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectShared.DTO
{
    [Serializable]
    public class UsersInTeamDTO
    {
        /// <summary>
        /// Null for users who do not participate in any teams
        /// </summary>
        [JsonProperty(PropertyName = "team_id")]
        public int? TeamId { get; set; }
        [JsonProperty(PropertyName = "team_name")]
        public string TeamName { get; set; }
        [JsonProperty(PropertyName = "users_in_team_ids")]
        public IEnumerable<int> UsersInTeamIds { get; set; }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + TeamId.GetHashCode();
            hash = (hash * 7) + TeamName.GetHashCode();
            foreach (var item in UsersInTeamIds)
                hash = (hash * 7) + item.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj)
        {
            return obj is UsersInTeamDTO && GetHashCode() == obj.GetHashCode();
        }
    }
}
