﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectShared.DTO
{
    public class TasksOfUserDTO
    {
        public int UserId { get; set; }
        /// <summary>
        /// Enumeration of IDs of tasks, where performer is user
        /// </summary>
        public IEnumerable<int> TasksOfUserIds { get; set; }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + UserId.GetHashCode();
            foreach (var item in TasksOfUserIds)
                hash = (hash * 7) + item.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj)
        {
            return obj is TasksOfUserDTO && GetHashCode() == obj.GetHashCode();
        }
    }
}
